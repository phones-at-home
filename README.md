# Phones At Home

Generative poetry.

- <https://mathr.co.uk/phones-at-home>
- <https://code.mathr.co.uk/phones-at-home>
- `git clone https://mathr.co.uk/phones-at-home.git`

## Minimal Deployment

```
$ sudo apt install \
  git \
  build-essential \
  make \
  wget
$ sudo adduser phones
$ cd /home/phones
$ sudo chown ${USER}:${USER} .
$ git clone https://code.mathr.co.uk/phones-at-home.git
$ cd phones-at-home
$ make
$ mkdir -p var/lib/phones-at-home var/log var/www/phones-at-home/book var/www/phones-at-home/zine
$ sudo chown -R phones:phones var/lib/phones-at-home var/log var/www/phones-at-home/book var/www/phones-at-home/zine
$ sudo chown phones:phones /home/phones
```

then `make forever` will generate poem text forever

## PDF Deployment

As Minimal Deployment, plus

```
$ sudo apt install \
  texlive-latex-recommended \
  texlive-humanities \
  lmodern \
  poppler-utils
```

then `make zine` and `make book` willl generate poem pdfs

## Podcast Deployment

As PDF Deployment, plus

```
$ sudo apt install \
  nginx # or other web server
$ su phones -c "crontab -e"
# m h  dom mon dow   command
0 3 * * 1-5 /home/phones/phones-at-home/etc/cron.daily/phones-at-home-zine
0 3 * * 6 /home/phones/phones-at-home/etc/cron.weekly/phones-at-home-book
$
# configure nginx or other web server to serve directory
/home/phones/phones-at-home/var/www/phones-at-home/
# at url
https://mathr.co.uk/phones-at-home/
# example config on phones-at-home-host server (raspberry pi)
$ cd /var/www/html
$ sudo ln -s /home/phones/phones-at-home/var/www/phones-at-home/
$
# example config on mathr.co.uk host (nginx reverse proxy)
    location /phones-at-home/ {
	proxy_http_version 1.1;
	proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
	proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_pass http://phones-at-home-host:80/phones-at-home/;
	proxy_redirect off;
    }
```

## Stream Deployment

As Minimal Deployment, plus

```
$ sudo modprobe snd-aloop
$ echo "snd-aloop" | sudo tee -a /etc/modules
$ sudo adduser phones audio
$ sudo apt install \
  curl \
  flite \
  darkice
# check that the alsa loopback device is card 2
# note: may become card 0 after reboot
$ su phones -c "aplay -l"
$ su phones -c "arecord -l"
# if not you will need to edit asoundrc and darkice.cfg
$ cp /home/phones/phones-at-home/etc/asoundrc /home/phones/.asoundrc
$ cp /home/phones/phones-at-home/etc/darkice.cfg.sample /home/phones/phones-at-home/etc/darkice.cfg
$ cp /home/phones/phones-at-home/etc/netrc.sample /home/phones/phones-at-home/etc/netrc
# make more secure (only readable by phones user)
$ sudo chown phones:phones /home/phones/phones-at-home/etc/darkice.cfg
$ sudo chown phones:phones /home/phones/phones-at-home/etc/netrc
$ sudo chmod 600 /home/phones/phones-at-home/etc/darkice.cfg
$ sudo chmod 600 /home/phones/phones-at-home/etc/netrc
# set password etc
$ sudo nano /home/phones/phones-at-home/etc/darkice.cfg
$ sudo nano /home/phones/phones-at-home/etc/netrc
$ su phones -c "crontab -e"
@reboot /home/phones/phones-at-home/bin/phones-at-home-stream
$ sudo reboot # or to run without rebooting:
$ su phones -c /home/phones/phones-at-home/bin/phones-at-home-stream
```

## Haskell Version

As Minimal Deployment, plus

```
$ sudo apt install \
  ghc \
  libghc-random-dev \
  dos2unix
$ cd phones-at-home
$ make bin/phones-at-home-hs share/phones-at-home/cmudict-0.7b.ascii
```

Not recommended for production due to slow speed and high memory usage.

The algorithm implementation may be easier to understand, however.

## Legal

Phones At Home -- generative poetry

Copyright (C) 2022  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

---
<https://mathr.co.uk>
