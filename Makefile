#!/usr/bin/env make
# Phones At Home -- generative poetry
# Copyright (C) 2022  Claude Heiland-Allen
# SPDX-License-Identifier: AGPL-3.0-only

SEED := $(shell date +%Y%m%d)
FILE := phones-at-home-$(SEED)

GHC = ghc
GCC = gcc

default: bin/phones-at-home share/phones-at-home/cmudict-0.7b

forever: bin/phones-at-home share/phones-at-home/cmudict-0.7b
	./bin/phones-at-home "$(SEED)" share/phones-at-home/cmudict-0.7b var/lib/phones-at-home/forbidden

pdf: zine book

zine: $(FILE)-zine.pdf

book: $(FILE)-book.pdf

%.pdf: %.tex
	pdflatex $<
	pdflatex $<

$(FILE)-zine.tex: bin/phones-at-home share/phones-at-home/cmudict-0.7b
	bin/phones-at-home "$(SEED)" share/phones-at-home/cmudict-0.7b var/lib/phones-at-home/forbidden 4 Zine > $@

$(FILE)-book.tex: bin/phones-at-home share/phones-at-home/cmudict-0.7b
	bin/phones-at-home "$(SEED)" share/phones-at-home/cmudict-0.7b var/lib/phones-at-home/forbidden 12 Book > $@

bin/phones-at-home-hs: src/phones-at-home.hs
	$(GHC) -O2 src/phones-at-home.hs -o bin/phones-at-home-hs

bin/phones-at-home: src/phones-at-home.c
	$(GCC) -std=c99 -Wall -Wextra -pedantic -O3 src/phones-at-home.c -o bin/phones-at-home -lm

share/phones-at-home/cmudict-0.7b.ascii: share/phones-at-home/cmudict-0.7b
	iconv -f ISO8859-1 -t ASCII//TRANSLIT < share/phones-at-home/cmudict-0.7b > share/phones-at-home/cmudict-0.7b.ascii
	dos2unix share/phones-at-home/cmudict-0.7b.ascii

share/phones-at-home/cmudict-0.7b:
	cd share/phones-at-home && wget https://raw.githubusercontent.com/Alexir/CMUdict/master/cmudict-0.7b
