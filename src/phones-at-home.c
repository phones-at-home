// Phones At Home -- generative poetry
// Copyright (C) 2022  Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-only

#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int popcount(uint64_t x)
{
  return __builtin_popcountll(x);
}

// xorshift64
// pre: x not zero
// post: x not zero
// return: new x
typedef uint64_t prng_t;
prng_t prng(prng_t x)
{
	x ^= x << 13;
	x ^= x >> 7;
	x ^= x << 17;
  return x;
}

// random number in range
// pre: x not zero, lo < hi
// post: x not zero
// return: lo <= r < hi
int64_t randomR(prng_t *x, int64_t lo, int64_t hi)
{
  *x = prng(*x);
  return floor(lo + (hi - lo) * ((double) *x) / ((double) ((prng_t)(1) << (CHAR_BIT * sizeof(prng_t) - 1)) * 2.0));
}

// enough to hold 3 chars
typedef uint32_t phone_t;

// enough to hold 39 bits (one per phone)
typedef uint64_t phone_mask_t;

// assume ASCII
char toLower(char c)
{
  if ('A' <= c && c <= 'Z')
  {
    return c - 'A' + 'a';
  }
  return c;
}

// a comment starts with ;;; and ends at line end
// pre: start of line, end of document
// post: start of line, end of document
// return: start was changed
bool comment(const char **start, const char *end)
{
  const char *skip = *start;
  if (skip < end && *skip == ';') skip++; else return false;
  if (skip < end && *skip == ';') skip++; else return false;
  if (skip < end && *skip == ';') skip++; else return false;
  while (skip < end && *skip != '\n') skip++;
  if (skip < end) skip++;
  *start = skip;
  return true;
}

// pre: start of line, end of document
// post: start of line, end of document
void comments(const char **start, const char *end)
{
  while (comment(start, end)) ;
}

// a word ends at the first space
// pre: start of word, end of document
// post start of word, end of word
// return: end was changed
bool word(const char *start, const char **end)
{
  const char *skip = start;
  while (skip < *end && *skip != ' ') skip++;
  if (skip < *end)
  {
    *end = skip;
    return true;
  }
  return false;
}

// a variant word ends in (...)
// pre: start of word, end of word
// post: start of word, end of base word
// return: end was changed
bool variant(const char *start, const char **end)
{
  const char *skip = *end - 1;
  if (start <= skip && *skip != ')') return false;
  do
  {
    --skip;
  } while (start <= skip && *skip != '(');
  if (start <= skip)
  {
    *end = skip;
    return true;
  }
  return false;
}

// a rhyme starts from the last vowel
// pre: start of phones including leading space, end of line
// post: start of rhyme, end of line
// return: start was changed
bool rhyme(const char **start, const char *end)
{
  const char *skip = end;
  do
  {
    --skip;
  } while (*start <= skip && *skip != '0' && *skip != '1' && *skip != '2');
  if (*start <= skip)
  {
    do
    {
      --skip;
    } while (*start <= skip && *skip != ' ');
    if (*start <= skip)
    {
      *start = skip + 1;
      return true;
    }
  }
  return false;
}

// a phone fits in a 3-char int
// pre: start of phone, end of line
// post: start of next phone, end of line
// return: the phone code
phone_t phone(const char **start, const char *end)
{
  const char *skip = *start;
  phone_t p = 0;
  while (skip < end && *skip != ' ' && *skip != '\r' && *skip != '\n')
  {
    p <<= CHAR_BIT;
    p |= (phone_t)(unsigned char)(*skip);
    ++skip;
  }
  if (skip < end && p)
  {
    *start = skip + 1;
    return p;
  }
  return 0;
}

// a vowel has 3 characters
bool isVowel(phone_t p)
{
  return p >= (((phone_t) 1) << (CHAR_BIT << 1));
}

// stress is stored in the lsb
// 0 unstressed -> 0
// 1 stressed -> 2
// 2 secondary -> 1
int stress(phone_t p)
{
  phone_t s = p & ((((phone_t) 1) << CHAR_BIT) - 1);
  switch (s)
  {
    case '0': return 0;
    case '1': return 2;
    case '2': return 1;
    default: return -1;
  }
}

// remove any stress from a phone
phone_t unstressed(phone_t p)
{
  if (isVowel(p)) return p >> CHAR_BIT; else return p;
}

// available phones
const char *phonesText = "AA AE AH AO AW AY B CH D DH EH ER EY F G HH IH IY JH K L M N NG OW OY P R S SH T TH UH UW V W Y Z ZH ";
#define PHONES 39
phone_t phoneList[PHONES];
void makePhones(void)
{
  const char *start = phonesText;
  const char *end = start + strlen(start);
  for (int i = 0; i < PHONES; ++i)
  {
    phoneList[i] = phone(&start, end);
    assert(phoneList[i]);
  }
  assert(start == end);
}

int phoneIndex(phone_t p)
{
  for (int i = 0; i < PHONES; ++i)
  {
    if (phoneList[i] == p)
    {
      return i;
    }
  }
  assert(! "found phone");
  return -1;
}

phone_mask_t phoneMask(phone_t p)
{
  return ((phone_mask_t) 1) << phoneIndex(unstressed(p));
}

int FORBIDDEN = 0;
phone_mask_t *forbiddenMask = 0;

bool forbidden(phone_mask_t m)
{
  for (int i = 0; i < FORBIDDEN; ++i)
  {
    if ((m & forbiddenMask[i]) == forbiddenMask[i])
    {
      return true;
    }
  }
  return false;
}

// all phones of a word
// pre: start of phones, end of line
// post: start of phones, end of line
// return: mask of phones in word
phone_mask_t phones(const char *start, const char *end)
{
  const char *skip = start;
  phone_mask_t m = 0;
  phone_t p;
  while ((p = phone(&skip, end)))
  {
    m |= phoneMask(p);
  }
  return m;
}

// count syllables
// pre: start of phones, end of line
// post: start of phones, end of line
// return: number of syllables
int syllables(const char *start, const char *end)
{
  const char *skip = start;
  int count = 0;
  phone_t p;
  while ((p = phone(&skip, end)))
  {
    if (isVowel(p))
    {
      count++;
    }
  }
  return count;
}

// rhythmic pattern
#define SYLLABLES 10

// score vs metric pattern
// pre: start of phones, end of line, target 0 or 2 for initial stress
// post: start of phones, end of line
// return: difference vs ideal meter
int meter(const char *start, const char *end, int target)
{
  const char *skip = start;
  int score = 0;
  phone_t p;
  while ((p = phone(&skip, end)))
  {
    if (isVowel(p))
    {
      score += abs(stress(p) - target);
      target = 2 - target;
    }
  }
  return score;
}

typedef uint64_t weight_t;
typedef struct
{
  phone_mask_t phones;
  // cmudict properties: length f < 2^32
  uint32_t word_start_offset;
  // cmudict properties: maximum (map length (lines f)) < 256
  uint8_t word_end_offset; // relative to word_start_offset
  uint8_t rhyme_start_offset; // relative to word_start_offset
  uint8_t rhyme_end_offset; // relative to word_start_offset
  uint8_t syllable_count;
  uint8_t score_ending_unstressed;
  uint8_t score_ending_stressed;
  // mutable state for filtering passes
  weight_t weight;
} word_t;

word_t makeWord(const char *global_start, const char *global_end, const char **word_start)
{
  word_t r = {0};
  assert(global_start <= *word_start);
  assert(*word_start < global_end);
  const char *word_end = global_end;
  if (! word(*word_start, &word_end))
  {
    return r;
  }
  // find phones
  const char *phones_start = word_end;
  while (phones_start < global_end && *phones_start == ' ')
  {
    phones_start++;
  }
  const char *phones_end = phones_start;
  while (phones_end < global_end && *phones_end != '\n')
  {
    phones_end++;
  }
  if (phones_end < global_end)
  {
    phones_end++;
  }
  // find last syllable
  const char *rhyme_start = word_end;
  const char *rhyme_end = phones_end;
  if (! rhyme(&rhyme_start, rhyme_end))
  {
    return r;
  }
  // strip trailing (...)
  variant(*word_start, &word_end);
  // populate return
  r.phones = phones(phones_start, phones_end);
  r.word_start_offset = *word_start - global_start;
  r.word_end_offset = word_end - *word_start;
  r.rhyme_start_offset = rhyme_start - *word_start;
  r.rhyme_end_offset = rhyme_end - *word_start;
  r.syllable_count = syllables(phones_start, phones_end);
  r.score_ending_stressed = meter(phones_start, phones_end, (r.syllable_count & 1) * 2);
  r.score_ending_unstressed = meter(phones_start, phones_end, (! (r.syllable_count & 1)) * 2);
  *word_start = phones_end;
  return r;
}

bool rhymesEqual(const char *start_a, const char *end_a, const char *start_b, const char *end_b)
{
  while (start_a < end_a && start_b < end_b)
  {
    phone_t a = unstressed(phone(&start_a, end_a));
    phone_t b = unstressed(phone(&start_b, end_b));
    if (a != b)
    {
      return false;
    }
    if (a == 0 && b == 0)
    {
      return true; // FIXME check if necessary
    }
  }
  return start_a == end_a && start_b == end_b;
}

#define THRESHOLD 3

weight_t calculateWeightsUnrhymed(word_t *words, int nwords, int endStressed, int syllablesAvailable, phone_mask_t requiredPhones)
{
  (void) syllablesAvailable;
  weight_t total = 0;
  for (int w = 0; w < nwords; ++w)
  {
    if ((words[w].phones & requiredPhones) != requiredPhones)
    {
      words[w].weight = 0;
    }
/*
    else if (words[w].syllable_count > syllablesAvailable)
    {
      words[w].weight = 0;
    }
*/
    else
    {
      int score = endStressed ? words[w].score_ending_stressed : words[w].score_ending_unstressed;
      if (score > THRESHOLD)
      {
        words[w].weight = 0;
      }
      else
      {
        words[w].weight = ((weight_t) 1) << (24 - score);
      }
    }
    total += words[w].weight;
  }
  return total;
}

weight_t calculateWeightsRhymed(word_t *words, int nwords, int endStressed, int syllablesAvailable, phone_mask_t requiredPhones, int rhymesWith, const char *start)
{
  (void) syllablesAvailable;
  const char *rhyme_start = start + words[rhymesWith].word_start_offset + words[rhymesWith].rhyme_start_offset;
  const char *rhyme_end   = start + words[rhymesWith].word_start_offset + words[rhymesWith].rhyme_end_offset;
  weight_t total = 0;
  for (int w = 0; w < nwords; ++w)
  {
    if ((words[w].phones & requiredPhones) != requiredPhones)
    {
      words[w].weight = 0;
    }
/*
    else if (words[w].syllable_count > syllablesAvailable)
    {
      words[w].weight = 0;
    }
*/
    else if (! rhymesEqual
      ( start + words[w].word_start_offset + words[w].rhyme_start_offset
      , start + words[w].word_start_offset + words[w].rhyme_end_offset
      , rhyme_start, rhyme_end
      ))
    {
      words[w].weight = 0;
    }
    else if (w == rhymesWith)
    {
      words[w].weight = 1;
    }
    else
    {
      int score = endStressed ? words[w].score_ending_stressed : words[w].score_ending_unstressed;
      if (score > THRESHOLD)
      {
        words[w].weight = 0;
      }
      else
      {
        words[w].weight = ((weight_t) 1) << (24 - score);
      }
    }
    total += words[w].weight;
  }
  return total;
}

int chooseWeighted(word_t *words, int nwords, weight_t target)
{
  weight_t total = 0;
  for (int w = 0; w < nwords; ++w)
  {
    total += words[w].weight;
    if (target < total)
    {
      assert(words[w].weight);
      return w;
    }
  }
  assert(! "chooseWeighted");
  return -1;
}

#define LINES 14
#define SYLLABLES 10
int poemWords[LINES][SYLLABLES];

int lineScore(word_t *words, int nwords, int line)
{
  int score = 0;
  int syllablesAvailable = SYLLABLES;
  for (int i = 0; i < SYLLABLES; ++i)
  {
    int w = poemWords[line][i];
    if (w != -1)
    {
      assert(0 <= w);
      assert(w < nwords);
      bool endStressed = ! (syllablesAvailable & 1);
      score += endStressed ? words[w].score_ending_stressed : words[w].score_ending_unstressed;
      syllablesAvailable -= words[w].syllable_count;
    }
  }
  return score;
}

bool lineUnrhymed(prng_t *prng, word_t *words, int nwords, phone_mask_t requiredPhones, int line)
{
  int syllablesAvailable, score;
  int totals[2] = { 0, 0 };
  do
  {
    // clear line
    for (int i = 0; i < SYLLABLES; ++i)
    {
      poemWords[line][i] = -1;
    }
    syllablesAvailable = SYLLABLES;
    int i = 0;
    while (i < SYLLABLES && syllablesAvailable > 0)
    {
      // pick word
      weight_t total = calculateWeightsUnrhymed(words, nwords, ! (syllablesAvailable & 1), syllablesAvailable, requiredPhones);
      if (total == 0)
      {
        if (i == 0)
        {
          return false;
        }
        else
        {
          if (++totals[! (syllablesAvailable & 1)] > 24)
          {
            return false;
          }
        }
        break;
      }
      weight_t target = randomR(prng, 0, total);
      int w = chooseWeighted(words, nwords, target);
      syllablesAvailable -= words[w].syllable_count;
      poemWords[line][i++] = w;
    }
    score = lineScore(words, nwords, line);
  } while (syllablesAvailable != 0 || score > THRESHOLD);
  return true;
}

bool lineRhymed(prng_t *prng, word_t *words, int nwords, phone_mask_t requiredPhones, int line, int rhymesWith, const char *start)
{
  int syllablesAvailable, score;
  int totals[2] = { 0, 0 };
  do
  {
    // clear line
    for (int i = 0; i < SYLLABLES; ++i)
    {
      poemWords[line][i] = -1;
    }
    syllablesAvailable = SYLLABLES;
    int i = 0;
    while (i < SYLLABLES && syllablesAvailable > 0)
    {
      // pick word
      weight_t total;
      if (i == 0)
      {
        total = calculateWeightsRhymed(words, nwords, ! (syllablesAvailable & 1), syllablesAvailable, requiredPhones, poemWords[rhymesWith][0], start);
      }
      else
      {
        total = calculateWeightsUnrhymed(words, nwords, ! (syllablesAvailable & 1), syllablesAvailable, requiredPhones);
      }
      if (total == 0)
      {
        if (i == 0)
        {
          return false;
        }
        else
        {
          if (++totals[! (syllablesAvailable & 1)] > 24)
          {
            return false;
          }
        }
        break;
      }
      weight_t target = randomR(prng, 0, total);
      int w = chooseWeighted(words, nwords, target);
      syllablesAvailable -= words[w].syllable_count;
      poemWords[line][i++] = w;
    }
    score = lineScore(words, nwords, line);
  } while (syllablesAvailable != 0 || score > THRESHOLD);
  return true;
}

bool poem(prng_t *prng, const char *start, word_t *words, int nwords, phone_mask_t required)
{
  return
  lineUnrhymed(prng, words, nwords, required, 0) &&
  lineUnrhymed(prng, words, nwords, required, 1) &&
  lineRhymed  (prng, words, nwords, required, 2, 0, start) &&
  lineRhymed  (prng, words, nwords, required, 3, 1, start) &&
  lineUnrhymed(prng, words, nwords, required, 4) &&
  lineUnrhymed(prng, words, nwords, required, 5) &&
  lineRhymed  (prng, words, nwords, required, 6, 4, start) &&
  lineRhymed  (prng, words, nwords, required, 7, 5, start) &&
  lineUnrhymed(prng, words, nwords, required, 8) &&
  lineUnrhymed(prng, words, nwords, required, 9) &&
  lineRhymed  (prng, words, nwords, required, 10, 8, start) &&
  lineRhymed  (prng, words, nwords, required, 11, 9, start) &&
  lineUnrhymed(prng, words, nwords, required, 12) &&
  lineRhymed  (prng, words, nwords, required, 13, 12, start);
}

void printPhone(int i)
{
  phone_t mask = (((phone_t) 1) << CHAR_BIT) - 1;
  phone_t p = phoneList[i];
  if (p < ((phone_t) 1) << CHAR_BIT)
  {
    putchar((char) (p & mask));
  }
  else if (p < ((phone_t) 1) << (CHAR_BIT << 1))
  {
    putchar((char) ((p >> CHAR_BIT) & mask));
    putchar((char) (p & mask));
  }
}

void printWord(const char *start, const char *end, const word_t *words, int nwords, int w)
{
  assert(start);
  assert(words);
  assert(0 <= w);
  assert(w < nwords);
  const char *word_start = start + words[w].word_start_offset;
  const char *word_end = word_start + words[w].word_end_offset;
  assert(start <= word_start);
  assert(start <= word_end);
  assert(word_start <= end);
  assert(word_end <= end);
  for (const char *p = word_start; p < word_end; ++p)
  {
    putchar(toLower(*p));
  }
}

const char punctuation[LINES+1] = ",:,.,;,.,?,!:.";

void plainText(const char *start, const char *end, const word_t *words, int nwords)
{
  for (int i = 0; i < LINES; ++i)
  {
    if ((i & 3) == 0)
    {
      printf("\n");
    }
    for (int j = SYLLABLES - 1; j >= 0; --j)
    {
      if (poemWords[i][j] < 0)
      {
        continue;
      }
      printWord(start, end, words, nwords, poemWords[i][j]);
      if (j == 0)
      {
        putchar(punctuation[i]);
      }
      else
      {
        putchar(' ');
      }
    }
    printf("\n");
  }
  printf("\n\n");
}

void putLatex(char c)
{
  switch (c)
  {
    case '_': // used in compound words
      putchar(' ');
      break;
    case '$': // need escaping
    case '&':
    case '%':
    case '#':
    case '{':
    case '}':
      putchar('\\');
      putchar(c);
      break;
    default:
      putchar(c);
      break;
  }
}

void latexWord(const char *start, const char *end, const word_t *words, int nwords, int w)
{
  assert(start);
  assert(end);
  assert(words);
  assert(0 <= w);
  assert(w < nwords);
  const char *word_start = start + words[w].word_start_offset;
  const char *word_end = word_start + words[w].word_end_offset;
  assert(start <= word_start);
  assert(start <= word_end);
  assert(word_start <= end);
  assert(word_end <= end);
  for (const char *p = word_start; p < word_end; ++p)
  {
    putLatex(toLower(*p));
  }
}

void latexLine(const char *start, const char *end, const word_t *words, int nwords, int i)
{
  for (int j = SYLLABLES - 1; j >= 0; --j)
  {
    if (poemWords[i][j] < 0)
    {
      continue;
    }
    latexWord(start, end, words, nwords, poemWords[i][j]);
    if (j == 0)
    {
      putchar(punctuation[i]);
    }
    else
    {
      putchar(' ');
    }
  }
}

void latexText(const char *start, const char *end, const word_t *words, int nwords, phone_mask_t requiredPhones)
{
  // find longest line
  int maxlength = 0, maxline = -1;
  for (int i = 0; i < LINES; ++i)
  {
    int length = 0;
    for (int j = 0; j < SYLLABLES; ++j)
    {
      int w = poemWords[i][j];
      if (w == -1)
      {
        break;
      }
      assert(0 <= w);
      assert(w < nwords);
      length += words[w].word_end_offset + 1;
    }
    if (length > maxlength)
    {
      maxlength = length;
      maxline = i;
    }
  }
  assert(0 <= maxline);
  assert(maxline < LINES);
  // title
  printf("\\newpage\n\\settowidth{\\versewidth}{");
  latexLine(start, end, words, nwords, maxline);
  printf("}\n\\centertitlesscheme\n\\poemtitle{\\textbf{");
  bool first = true;
  for (int i = 0; i < PHONES; ++i)
  {
    if (requiredPhones & (((phone_mask_t) 1) << i))
    {
      if (! first)
      {
        printf(" ");
      }
      printPhone(i);
      first = false;
    }
  }
  // poem
  printf("}}\n\\begin{poem}[\\versewidth]\n");
  for (int i = 0; i < 12; i += 4)
  {
    printf("\\begin{stanza}\n");
    latexLine(start, end, words, nwords, i + 0);
    printf(" \\verseline\n\\verseindent ");
    latexLine(start, end, words, nwords, i + 1);
    printf(" \\verseline\n");
    latexLine(start, end, words, nwords, i + 2);
    printf(" \\verseline\n\\verseindent ");
    latexLine(start, end, words, nwords, i + 3);
    printf("\n\\end{stanza}\n");
  }
  printf("\\begin{stanza}\n\\verseindent ");
  latexLine(start, end, words, nwords, 12);
  printf(" \\verseline\n\\verseindent ");
  latexLine(start, end, words, nwords, 13);
  printf("\n\\end{stanza}\n\\end{poem}\n\n");
}

int cmp_int_asc(const void *a, const void *b)
{
  const int *p = a;
  const int *q = b;
  int x = *p;
  int y = *q;
  return (x > y) - (x < y);
}

phone_mask_t choosePhones(prng_t *prng, int count)
{
  assert(0 <= count);
  assert(count <= PHONES);
  phone_mask_t m = 0;
  do
  {
    int choice[PHONES];
    for (int i = 0; i < count; ++i)
    {
      qsort(choice, i, sizeof(*choice), cmp_int_asc);
      int p = randomR(prng, 0, PHONES - i);
      for (int j = 0; j < i; ++j)
      {
        if (choice[j] <= p)
        {
          ++p;
          assert(p < PHONES);
        }
      }
      choice[i] = p;
    }
    // check
    int counts[PHONES];
    for (int i = 0; i < PHONES; ++i)
    {
      counts[i] = 0;
    }
    for (int i = 0; i < count; ++i)
    {
      ++counts[choice[i]];
    }
    for (int i = 0; i < PHONES; ++i)
    {
      assert(counts[i] <= 1);
    }
    // make mask
    m = 0;
    for (int i = 0; i < count; ++i)
    {
      m |= ((phone_mask_t) 1) << choice[i];
    }
  } while (popcount(m) != count || forbidden(m));
  return m;
}

bool readFile(const char *filename, const char **start, const char **end)
{
  assert(filename);
  assert(start);
  assert(end);
  FILE *f = fopen(filename, "rb");
  if (! f)
  {
    return false;
  }
  if (0 != fseek(f, 0, SEEK_END))
  {
    fclose(f);
    return false;
  }
  long bytes = ftell(f);
  if (bytes < 0)
  {
    fclose(f);
    return false;
  }
  rewind(f);
  char *buffer = malloc(bytes);
  if (! buffer)
  {
    fclose(f);
    return false;
  }
  if (1 != fread(buffer, bytes, 1, f))
  {
    free(buffer);
    fclose(f);
    return false;
  }
  fclose(f);
  *start = buffer;
  *end = buffer + bytes;
  return true;
}

bool parseDictionary(const char *start, const char *end, word_t **words, int *nwords)
{
  const char *skip = start;
  comments(&skip, end);
  const char *p = skip;
  int count = 0;
  while (p < end)
  {
    count += (*p++ == '\n');
  }
  word_t *buffer = calloc(1, sizeof(*buffer) * count);
  if (! buffer)
  {
    assert(buffer);
    return false;
  }
  for (int w = 0; w < count; ++w)
  {
    buffer[w] = makeWord(start, end, &skip);
    if (buffer[w].phones == 0)
    {
      assert(buffer[w].phones);
      free(buffer);
      return false;
    }
  }
  if (skip != end)
  {
    assert(skip == end);
    free(buffer);
    return false;
  }
  *words = buffer;
  *nwords = count;
  return true;
}

bool parseForbidden(const char *start, const char *end)
{
  if (start == end)
  {
    return true;
  }
  // count newlines and replace with spaces
  int count = 0;
  char *skip = (void *) start; // const cast
  char *kend = (void *) end; // const cast
  while (skip < kend)
  {
    if (*skip == '\n')
    {
      ++count;
      *skip = ' ';
    }
    ++skip;
  }
  // allocate buffer
  phone_mask_t *buffer = calloc(1, sizeof(*buffer) * count);
  if (! buffer)
  {
    return false;
  }
  for (int j = 0; j < count; ++j)
  {
    buffer[j] = 0;
    for (int i = 0; i < 2; ++i)
    {
      phone_t p = phone(&start, end);
      if (! p)
      {
        free(buffer);
        return false;
      }
      buffer[j] |= phoneMask(p);
    }
  }
  FORBIDDEN = count;
  forbiddenMask = buffer;
  return true;
}

int main(int argc, char **argv)
{
  makePhones();
  if (argc == 4 || argc == 5 || argc == 6 || argc == 7)
  {
    prng_t seed = atoll(argv[1]);
    for (int i = 0; i < 256; ++i)
    {
      seed = prng(seed);
    }
    const char *start = 0, *end = 0;
    if (! readFile(argv[2], &start, &end))
    {
      fprintf(stderr, "%s: could not load dictionary: %s\n", argv[0], argv[2]);
      return 1;
    }
    word_t *words = 0; int nwords = 0;
    if (! parseDictionary(start, end, &words, &nwords))
    {
      fprintf(stderr, "%s: could not parse dictionary: %s\n", argv[0], argv[2]);
      free((void *) start); // const cast
      return 1;
    }
    const char *forbidden_start = 0, *forbidden_end = 0;
    if (readFile(argv[3], &forbidden_start, &forbidden_end))
    {
      if (! parseForbidden(forbidden_start, forbidden_end))
      {
        fprintf(stderr, "%s: could not parse forbidden: %s\n", argv[0], argv[3]);
        free((void *) forbidden_start); // const cast
        free((void *) start); // const cast
        return 1;
      }
      free((void *) forbidden_start); // const cast
    }
    int count = argc >= 5 ? atoi(argv[4]) : 0;
    if (argc == 6)
    {
      phone_mask_t palette;
      if (count > PHONES)
      {
        palette = (((phone_mask_t) 1) << PHONES) - 1;
      }
      else
      {
        palette = choosePhones(&seed, count);
      }
      printf
        (
"\\documentclass[11pt,twoside]{scrartcl}\n"
"\\usepackage[paperwidth=105mm,paperheight=148mm,inner=10mm,outer=10mm,top=20mm,bottom=20mm]{geometry}\n"
"\\usepackage[colorlinks=true,linkcolor=black,anchorcolor=black,citecolor=black,filecolor=black,menucolor=black,runcolor=black,urlcolor=black,pdftex,pdfauthor={Claude Heiland-Allen},pdftitle={Phones At Home %s \\#%s},pdfsubject={Poetry},pdfkeywords={"
, argv[5], argv[1]
        );
      bool first = true;
      for (int i = 0; i < PHONES; ++i)
      {
        if (palette & (((phone_mask_t) 1) << i))
        {
          if (! first)
          {
            putchar(',');
          }
          printPhone(i);
          first = false;
        }
      }
      printf
        (
"}]{hyperref}\n"
"\\usepackage{lmodern,fancyhdr,ifthen,keyval,poemscol}"
"\\renewcommand{\\familydefault}{\\sfdefault}"
"\\title{We Have \\\\ Phones At Home}"
"\\subtitle{Phones At Home: \\\\"
        );
      for (int i = 0; i < PHONES; ++i)
      {
        if (palette & (((phone_mask_t) 1) << i))
        {
          putchar(' ');
          printPhone(i);
          first = false;
        }
      }
      printf
        (
"}\n"
"\\author{generated using code by \\\\ Claude Heiland-Allen \\\\ \\url{mailto:claude@mathr.co.uk}}\n"
"\\date{\\#%s}\n"
"\\pagestyle{empty}\n"
"\\parindent 0pt\n"
"\\parskip 5pt\n"
"\\global\\verselinenumbersfalse\n"
"\\begin{document}\n"
"\\maketitle\n"
"\\thispagestyle{empty}\n\n"
, argv[1]
       );
      for (int i = 0; i < PHONES; ++i)
      {
        if (palette & (((phone_mask_t) 1) << i))
        {
          for (int j = i + 1; j < PHONES; ++j)
          {
            if (palette & (((phone_mask_t) 1) << j))
            {
              phone_mask_t requiredPhones = (((phone_mask_t) 1) << i) | (((phone_mask_t) 1) << j);
              putchar('%');
              putchar(' ');
              printPhone(i);
              putchar(' ');
              printPhone(j);
              putchar('\n');
              fflush(stdout);
              if (forbidden(requiredPhones))
              {
                continue;
              }
              if (poem(&seed, start, words, nwords, requiredPhones))
              {
                latexText(start, end, words, nwords, requiredPhones);
                fflush(stdout);
              }
              else
              {
                return 1;
              }
            }
          }
        }
      }
      printf
        (
"\\newpage\n"
"\\section*{Credits}\n"
"\n"
"The Carnegie~Mellon~University \\\\ Pronouncing~Dictionary \\\\\n"
"\\url{http://www.speech.cs.cmu.edu/cgi-bin/cmudict}\n"
"\n"
"custom C code compiled using \\\\ The~GNU~Compiler~Collection \\\\\n"
"\\url{https://gcc.gnu.org/}\n"
"\n"
"typeset in Latin~Modern~Sans using TeX~Live \\\\\n"
"\\url{https://tug.org/texlive/} \\\\\n"
"with packages \\verb|fancyhdr|,  \\verb|geometry|,  \\verb|hyperref|,  \\verb|ifthen|,  \\verb|keyval|,  \\verb|lmodern|,  \\verb|poemscol|, \\verb|scrartcl|\n"
"\n"
"\\section*{Source}\n"
"\n"
"\\url{https://mathr.co.uk/phones-at-home}\n"
"\n"
"\\url{https://code.mathr.co.uk/phones-at-home}\n"
"\n"
"\\verb|git clone \\| \\\\\n"
"\\verb|  https://code.mathr.co.uk/phones-at-home.git|\n"
"\n"
"\\end{document}\n"
        );
    }
    else
    {
      int j = 0;
      while (count == 0 || j++ < count)
      {
        phone_mask_t requiredPhones;
        if (argc == 7)
        {
          requiredPhones = (((phone_mask_t) 1) << atoi(argv[5])) | (((phone_mask_t) 1) << atoi(argv[6]));
          if (! forbidden(requiredPhones))
          {
            return 0;
          }
        }
        else
        {
          requiredPhones = choosePhones(&seed, 2);
        }
        printf("We Have Phones At Home.\nPhones At Home:");
        for (int i = 0; i < PHONES; ++i)
        {
           if (requiredPhones & (((phone_mask_t) 1) << i))
          {
            printf(" ");
            printPhone(i);
          }
        }
        printf(".\n");
        fflush(stdout);
        if (poem(&seed, start, words, nwords, requiredPhones))
        {
          plainText(start, end, words, nwords);
          fflush(stdout);
        }
        else
        {
          return 1;
        }
      }
    }
    if (forbiddenMask)
    {
      free(forbiddenMask);
    }
    free(words);
    free((void *) start); // const cast
    return 0;
  }
  else
  {
    fprintf(stderr, "usage: %s seed cmudict forbidden [count]\n", argv[0]);
    fprintf(stderr, "usage: %s seed cmudict forbidden count title\n", argv[0]);
    fprintf(stderr, "usage: %s seed cmudict forbidden count phone1 phone2\n", argv[0]);
  }
  return 1;
}
