-- Phones At Home -- generative poetry
-- Copyright (C) 2022  Claude Heiland-Allen
-- SPDX-License-Identifier: AGPL-3.0-only

module Main (main) where

import Data.Array (array, (!)) -- package array
import Data.Char (toLower)
import Data.List (intercalate, isPrefixOf, nub, sort, sortBy, tails)
import Data.Ord (comparing)
import System.Environment (getArgs)
import System.Exit (exitFailure)
import System.IO (BufferMode(LineBuffering), hSetBuffering, stdout)
import System.Random (StdGen, mkStdGen, randomR) -- package random

-- available phones
phones :: [String]
phones = words "AA AE AH AO AW AY B CH D DH EH ER EY F G HH IH IY JH K L M N NG OW OY P R S SH T TH UH UW V W Y Z ZH"

-- words with both of these phonemes are too rare
forbidden :: [(String, String)]
forbidden = [("AW", "UH"), ("AW", "ZH"), ("CH", "DH"), ("CH", "ZH"), ("DH", "HH"), ("DH", "JH"), ("DH", "OY"), ("DH", "TH"), ("DH", "UH"), ("DH", "ZH"), ("JH", "ZH"), ("SH", "ZH")]

-- parse the CMU rhyming dictionary
dictionary :: String -> [(String, [String])]
dictionary = map (\l -> case words l of (w:p) -> (w,p)) . filter (not . isPrefixOf ";;;") . lines

-- does a pronounciation contain a phone
contains :: String -> (String, [String]) -> Bool
contains phone (_, pron) = phone `elem` map unstressed pron

-- filter a dictionary to words containing all phones
restrict :: [(String, [String])] -> [String] -> [(String, [String])]
restrict dict ps = filter (\s -> all (`contains` s) ps) dict

-- get the vowel stress of phones
stress :: [String] -> [Int]
stress = map foot . filter (`elem` "012") . concat

-- remove vowel stress
unstressed :: String -> String
unstressed = filter (`notElem` "012")

-- vowels are labelled with stress
vowel :: String -> Bool
vowel v = v /= unstressed v

-- convert stress to semantic number
foot :: Char -> Int
foot '0' = 0 -- unstressed
foot '1' = 2 -- primary stress
foot '2' = 1 -- secondary stress

-- count syllables (number of vowels)
syllables :: [String] -> Int
syllables = length . stress

-- iambic pentameter
meter :: [Int]
meter = map foot "0101010101"

-- is the stress of a list of words close to the meter?
metric :: Int -> [Int] -> [(String, [String])] -> Bool
metric threshold m = (<= threshold) . edit_distance m . concatMap (stress . snd)

-- get the terminating syllable
ending :: [String] -> [String]
ending ps = case break vowel (reverse ps) of
  (ks, v:_) -> reverse (ks ++ [unstressed v])
  (ks, []) -> reverse ks

-- check if a pronounciation ends with something
endsWith :: Maybe [String] -> (String, [String]) -> Bool
endsWith Nothing _ = True
endsWith (Just e) (_, ps) = e == ending ps

-- find edit distance between two stress lists
edit_distance :: [Int] -> [Int] -> Int
edit_distance a b = d!(m,n)
  where
    d = array ((0,0), (m, n)) ([((0,0), 0)] ++
          [((i,0), sum [ w_del ak | ak <- take i a]) | i <- [1..m]] ++
          [((0,j), sum [ w_ins bk | bk <- take j b]) | j <- [1..n]] ++
          [((i,j), if ai == bj then d!(i-1,j-1) else minimum [ d!(i-1,j) + w_del ai, d!(i,j-1) + w_ins bj, d!(i-1,j-1) + w_sub ai bj ]) | (ai,i) <- zip a [1..], (bj,j) <- zip b [1..]])
    m = length a
    n = length b
    w_del x = x + 10000 -- effectively forbidden
    w_ins x = x + 10000 -- effectively forbidden
    w_sub x y = abs (x - y) -- changes should be small

-- pick uniformly at random
choose :: StdGen -> [a] -> (StdGen, a)
choose g xs = case randomR (0, length xs - 1) g of
  (r, g') -> (g', xs !! r)

-- generate a line satisfying line ending and meter
gen :: StdGen -> [(String, [String])] -> Maybe [String] -> [(String, [String])] -> (StdGen, [(String, [String])])
gen g dict end acc = case sum (map (syllables . snd) acc)  `compare` length meter of
  EQ  | metric threshold meter acc -> (g, acc)
      | otherwise -> gen g dict end []
  GT -> gen g dict end []
  LT -> let (g', w) = choose g (filter (endsWith (if null acc then end else Nothing)) dict)
        in  gen g' dict end (w:acc)
  where
    threshold = 3 -- FIXME

-- generate a sonnet (iambic pentameter with ABAB CDCD EFEF GG rhyme scheme)
sonnet :: StdGen -> [(String, [String])] -> (StdGen, [[(String, [String])]])
sonnet g0 dict = (g14, [l1,l2,l3,l4,l5,l6,l7,l8,l9,l10,l11,l12,l13,l14])
  where
    (g1, l1) = line g0 Nothing
    (g2, l2) = line g1 Nothing
    (g3, l3) = line g2 (ending' l1)
    (g4, l4) = line g3 (ending' l2)
    (g5, l5) = line g4 Nothing
    (g6, l6) = line g5 Nothing
    (g7, l7) = line g6 (ending' l5)
    (g8, l8) = line g7 (ending' l6)
    (g9, l9) = line g8 Nothing
    (g10, l10) = line g9 Nothing
    (g11, l11) = line g10 (ending' l9)
    (g12, l12) = line g11 (ending' l10)
    (g13, l13) = line g12 Nothing
    (g14, l14) = line g13 (ending' l13)
    line g e = gen g dict e []
    ending' = Just . ending . (concatMap snd)

-- remove any suffix in parentheses
stripSuffixBrackets :: String -> String
stripSuffixBrackets = reverse . stripPrefixBrackets . reverse

-- remove a prefix in swapped parenthesis
stripPrefixBrackets :: String -> String
stripPrefixBrackets (')':xs) = drop 1 (dropWhile (/= '(') xs)
stripPrefixBrackets xs = xs

-- typeset a poem in plain text
plainset :: (String, [String]) -> String
plainset (title, ls@[l1,l2,l3,l4,l5,l6,l7,l8,l9,l10,l11,l12,l13,l14]) = unlines $
  [ "We Have Phones At Home."
  , "Phones At Home: " ++ title ++ "."
  , ""
  , l1 ++ ","
  , l2 ++ ":"
  , l3 ++ ","
  , l4 ++ "."
  , ""
  , l5 ++ ","
  , l6 ++ ";"
  , l7 ++ ","
  , l8 ++ "."
  , ""
  , l9 ++ ","
  , l10 ++ "?"
  , l11 ++ ","
  , l12 ++ "!"
  , ""
  , l13 ++ ":"
  , l14 ++ "."
  , ""
  ]
  where
    long = last $ sortBy (comparing length) ls

-- typeset a poem in LaTeX
typeset :: (String, [String]) -> String
typeset (title, ls@[l1,l2,l3,l4,l5,l6,l7,l8,l9,l10,l11,l12,l13,l14]) = unlines $
  [ "\\newpage"
  , "\\settowidth{\\versewidth}{" ++ long ++ "}"
  , "\\centertitlesscheme"
  , "\\poemtitle{\\textbf{" ++ title ++ "}}"
  , "\\begin{poem}[\\versewidth]"
  , "\\begin{stanza}"
  , l1 ++ " \\verseline"
  , "\\verseindent " ++ l2 ++ " \\verseline"
  , l3 ++ " \\verseline"
  , "\\verseindent " ++ l4
  , "\\end{stanza}"
  , "\\begin{stanza}"
  , l5 ++ " \\verseline"
  , "\\verseindent " ++ l6 ++ " \\verseline"
  , l7 ++ " \\verseline"
  , "\\verseindent " ++ l8
  , "\\end{stanza}"
  , "\\begin{stanza}"
  , l9 ++ " \\verseline"
  , "\\verseindent " ++ l10 ++ " \\verseline"
  , l11 ++ " \\verseline"
  , "\\verseindent " ++ l12
  , "\\end{stanza}"
  , "\\begin{stanza}"
  , "\\verseindent " ++ l13 ++ " \\verseline"
  , "\\verseindent " ++ l14
  , "\\end{stanza}"
  , "\\end{poem}"
  ]
  where
    long = last $ sortBy (comparing length) ls

-- LaTeX boilerplate
document :: [String] -> String -> String -> String
document ps date body = unlines $
  [ "\\documentclass[11pt,twoside]{scrartcl}"
  , "\\usepackage[paperwidth=105mm,paperheight=148mm,inner=10mm,outer=10mm,top=20mm,bottom=20mm]{geometry}"
  , "\\usepackage[colorlinks=true,linkcolor=black,anchorcolor=black,citecolor=black,filecolor=black,menucolor=black,runcolor=black,urlcolor=black,pdftex,pdfauthor={Claude Heiland-Allen},pdftitle={Phones At Home Zine \\#" ++ date ++ "},pdfsubject={Poetry},pdfkeywords={" ++ intercalate "," ps ++ "}]{hyperref}"
  , "\\usepackage{lmodern,fancyhdr,ifthen,keyval,poemscol}"
  , "\\renewcommand{\\familydefault}{\\sfdefault}"
  , "\\title{We Have \\\\ Phones At Home}"
  , "\\subtitle{Phones At Home: \\\\ " ++ unwords ps ++ "}"
  , "\\author{generated using code by \\\\ Claude Heiland-Allen \\\\ \\url{mailto:claude@mathr.co.uk}}"
  , "\\date{\\#" ++ date ++ "}"
  , "\\pagestyle{empty}"
  , "\\parindent 0pt"
  , "\\parskip 5pt"
  , "\\global\\verselinenumbersfalse"
  , "\\begin{document}"
  , "\\maketitle"
  , "\\thispagestyle{empty}"
  , body
  , "\\newpage"
  , "\\section*{Credits}"
  , ""
  , "The Carnegie~Mellon~University \\\\ Pronouncing~Dictionary \\\\"
  , "\\url{http://www.speech.cs.cmu.edu/cgi-bin/cmudict}"
  , ""
  , "custom Haskell code compiled using \\\\ The~Glasgow~Haskell~Compiler \\\\"
  , "\\url{https://www.haskell.org/ghc/} \\\\"
  , "with packages \\verb|array|, \\verb|base|, \\verb|random|"
  , ""
  , "typeset in Latin~Modern~Sans using TeX~Live \\\\"
  , "\\url{https://tug.org/texlive/} \\\\"
  , "with packages \\verb|fancyhdr|,  \\verb|geometry|,  \\verb|hyperref|,  \\verb|ifthen|,  \\verb|keyval|,  \\verb|lmodern|,  \\verb|poemscol|, \\verb|scrartcl|"
  , ""
  , "\\section*{Source}"
  , ""
  , "\\url{https://mathr.co.uk/phones-at-home}"
  , ""
  , "\\url{https://code.mathr.co.uk/phones-at-home}"
  , ""
  , "\\verb|git clone \\| \\\\"
  , "\\verb|  https://code.mathr.co.uk/phones-at-home.git|"
  , ""
  , "\\end{document}"
  ]

-- repeat a random generator a fixed number of times
times :: Int -> (StdGen -> (StdGen, a)) -> StdGen -> (StdGen, [a])
times 0 _ g = (g, [])
times n f g = case times (n-1) f g of (g', as) -> case f g' of (g'', a) -> (g'', a:as)

-- choose a set of phones without forbidden pairs
choosePhones :: Int -> StdGen -> (StdGen, [String])
choosePhones n g = case times n (`choose` phones) g of
  (g', ps)
    | length (nub ps) == n && null [(p,q)|p<-ps,q<-ps,(p,q)`elem`forbidden] -> (g', ps)
    | otherwise -> choosePhones n g'

-- generate poems for a set of phones
poems :: [(String, [String])] -> [String] -> StdGen -> (StdGen, [(String, [String])])
poems dict ps g0 = go g0 [[p,q]|p:qs<-tails ps,q<-qs] []
  where
    go g [] acc = (g, reverse acc)
    go g (p:qs) acc = case sonnet g (restrict dict p) of
      (g', s) -> go g' qs ((unwords p, map (format texSafe) s):acc)

-- get the text of a poem
format :: (Char -> String) -> [(String, [String])] -> String
format safe = concatMap (safe . toLower) . unwords . map (stripSuffixBrackets . fst)

-- replace characters with LaTeX-safe equivalents
texSafe :: Char -> String
texSafe '_' = " " -- used in compound words in cmudict
texSafe '$' = "\\$"
texSafe '&' = "\\&"
texSafe '%' = "\\%"
texSafe '#' = "\\#"
texSafe '{' = "\\{"
texSafe '}' = "\\}"
texSafe c = [c]

-- replace characters with plain-safe equivalents
plainSafe :: Char -> String
plainSafe '_' = " " -- used in compound words in cmudict
plainSafe '’' = "'" -- don't break text-to-speech
plainSafe c = [c]

-- generate a book of sonnets
main :: IO ()
main = do
  args <- getArgs
  case args of
    [count, seed, date, cmudict] -> do -- generate LaTeX document
      dict <- fmap dictionary $ readFile cmudict
      let g0 = mkStdGen (read seed)
          (g1, ps) = choosePhones (read count) g0
          (_g2, qs) = poems dict (sort ps) g1
          tex = document (sort ps) date (unlines (map typeset qs))
      putStr tex
    [seed, cmudict] -> do -- generate plain text forever
      dict <- fmap dictionary $ readFile cmudict
      let go g0 =
            let (g1, ps) = choosePhones 2 g0
                (g2, qs) = poems dict (sort ps) g1
            in  unlines (map plainset qs) ++ go g2
      hSetBuffering stdout LineBuffering
      putStr $ go (mkStdGen (read seed))
    _ -> exitFailure
